function map(elements, cb) {
	if (elements === undefined || elements.length === 0) return [];
	let arr = elements;
	let res = [];
	for (let i = 0; i < arr.length; i++) {
		if (arr[i] !== undefined) res.push(cb(arr[i], i, elements));
	}
	return res;
}
module.exports = map;
