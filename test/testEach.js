const data = require("../data");
const each = require("../each.js");
function callBack(value, index, array) {
  console.log(value, index);
}
const res = each(data, callBack);
if (res) {
  console.log(res);
}
