const data = require("../data");
const reduce = require("../reduce.js");
let startingValue = undefined;
function callBack(accumulator, currentValue, index, array) {
  accumulator += currentValue;
  return accumulator;
}

const res = reduce(data, callBack, startingValue);

console.log(res);
