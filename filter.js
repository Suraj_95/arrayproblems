function filter(elements, cb) {
	if (elements === undefined || elements.length === 0) return [];
	let arr = [];
	for (let i = 0; i < elements.length; ++i) {
		if (cb(elements[i], i, elements)) {
			arr.push(elements[i]);
		}
	}
	return arr;
}
module.exports = filter;
