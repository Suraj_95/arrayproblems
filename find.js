function find(data, cb) {
	if (data === undefined || data.length === 0) return [];
	let found = undefined;
	for (let i = 0; i < data.length; ++i) {
		if (cb(data[i], i, data)) {
			return (found = data[i]);
		}
	}
	return found;
}
module.exports = find;
