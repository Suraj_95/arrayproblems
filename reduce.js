function reduce(elements, callBack, startingValue) {
	let arr = elements;
	if (elements === undefined || elements.length === 0) return [];
	if (startingValue === undefined) {
		startingValue = elements[0];
		arr = arr.slice(1, elements.length);
	}
	let accumulator = startingValue;
	for (let i = 0; i < arr.length; i++) {
		if (arr[i] !== undefined) {
			accumulator = callBack(accumulator, arr[i], i, elements);
		}
	}
	return accumulator;
}
module.exports = reduce;
