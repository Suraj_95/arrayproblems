let res = [];
function flatten(elements) {
	if (elements === undefined || elements.length == 0) return [];
	for (let i = 0; i < elements.length; i++) {
		removeArray(elements[i]);
	}

	function removeArray(item) {
		if (Array.isArray(item)) {
			flatten(item);
		} else {
			//undefined is false in nature.
			if (item !== undefined) {
				res.push(item);
			}
		}
	}
	return res;
}

module.exports = flatten;
